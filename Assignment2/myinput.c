#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "myinput.h"

/* Internal read buffer grow size (used for "realloc(3)") */
const unsigned grow_size = 1024;

/* Internal tokenizer object */
struct tokenizer
{
    int fd; /* File descriptor to read data from */
    char *buffer; /* Working buffer */
    unsigned size; /* Currently allocated space for buffer */
    unsigned amount; /* Amount of data read, but unprocessed yet */
    unsigned token_length; /* Token value length from beginning of the buffer */
};

/* stop callback for "read_token" proc: stop when a given char is encountered */
int stop_until(char ch, void *stop_ctx)
{
    return ch == *((char *)stop_ctx) ? 1 : 0;
}

/* stop callback for "read_token" proc: stop when a whitespace char is encountered */
int stop_whitespace(char ch, void *stop_ctx)
{
    return ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n' ? 1 : 0;
}

/* stop callback for "read_token" proc: stop when a given amount of data is read */
int stop_count(char ch, void *stop_ctx)
{
    unsigned *counter = ((unsigned *)stop_ctx);
    if (*counter == 0) 
        return 1;

    (*counter)--;
    return 0;
}

/* generic token reader: reads data into buffer (if needs to), parses it and returns a token value parsed */
int read_token(tokenizer_t tok, struct token *token, int (*stop)(char ch, void *stop_ctx), void *stop_ctx, char **error_message)
{
    if (tok == NULL)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "null tokenizer pointer passed for read_token");
        }

        return 1;
    }

    if (tok->token_length > 0)
    {
        memmove(tok->buffer, tok->buffer + tok->token_length, tok->amount - tok->token_length);
        tok->amount -= tok->token_length;
    }

    int token_read = 0;
    tok->token_length = 0;

    while (token_read == 0)
    {
        /* Check if there is not enough data in buffer currently */
        if (tok->token_length == tok->amount || tok->amount >= tok->size)
        {
            /* Allocate more space */
            tok->buffer = realloc(tok->buffer, tok->size + grow_size);
            if (tok->buffer == NULL)
            {
                if (error_message != NULL)
                {
                    if (*error_message != NULL)
                        free(error_message);
                    asprintf(error_message, "no memory for tokenizer buffer");
                }
                
                return 1;
            }
            
            tok->size += grow_size;

            /* Read more data */
            ssize_t bread = read(tok->fd, tok->buffer + tok->amount, (size_t)(tok->size - tok->amount));
            if (bread < 0)
            {
                if (error_message != NULL)
                {
                    if (*error_message != NULL)
                        free(error_message);
                    asprintf(error_message, "read(2) failed with error: %s (errno = %d)", strerror(errno), errno);
                }

                return 1;
            }

            if (bread == 0)
                break;
            
            tok->amount += bread;
        }

        if (tok->token_length == tok->amount)
            break;

        /* Parse data using stop condition handled by "stop" callback */
        for (; tok->token_length < tok->amount; tok->token_length++)
            if (stop(tok->buffer[tok->token_length], stop_ctx) != 0)
            {
                token_read = 1;
                break;
            }
    }

    if (token == NULL)
        return 0;

    token->text = tok->buffer;
    token->text_length = tok->token_length;

    /* trim left */
    for (; (token->text_length > 0) && (token->text[0] == ' ' || token->text[0] == '\n'); token->text++, token->text_length--);
    /* trim right */
    for (; (token->text_length > 0) &&
             (token->text[token->text_length - 1] == ' ' || token->text[token->text_length - 1] == '\n');
         token->text_length--);
    
    return 0;
}

int read_token_until(tokenizer_t tok, struct token *token, char stop_char, char **error_message)
{
    return read_token(tok, token, stop_until, &stop_char, error_message);
}

int read_token_exact(tokenizer_t tok, struct token *token, unsigned count, char **error_message)
{
    return read_token(tok, token, stop_count, &count, error_message);
}

int read_token_until_whitespace(tokenizer_t tok, struct token *token, char **error_message)
{
    return read_token(tok, token, stop_whitespace, NULL, error_message);
}

int make_tokenizer(int fd, struct tokenizer **tok, char **error_message)
{
    if (tok == NULL)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "null tokenizer pointer passed for make_tokenizer");
        }

        return 1;
    }

    *tok = malloc(sizeof(struct tokenizer));
    if (*tok == NULL)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "no memory for tokenizer");
        }

        return 1;
    }
    memset(*tok, 0, sizeof(struct tokenizer));
    (*tok)->fd = fd;
    
    return 0;
}

void free_tokenizer(struct tokenizer **tok)
{
    if ((tok == NULL) || (*tok == NULL))
        return;
    if ((*tok)->buffer != NULL)
        free((*tok)->buffer);
    free(*tok);
    *tok = NULL;
}

