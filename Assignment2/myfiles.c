#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

/* Apply "stat(2)" for given filename and analyze it's result.
 * The function will succeed only if file exists, and there is enough permissions to access it.
 */
int check_file_stats(const char *filename, char **error_message)
{
    struct stat st;
    memset(&st, 0, sizeof(st));

    if (stat(filename, &st) != 0)
    {
        if (error_message == NULL)
            return 1;
        if (*error_message != NULL)
            free(error_message);
        
        if (errno == ENOENT)
            asprintf(error_message, "file '%s' does not exist", filename);
        else if (errno == EACCES)
            asprintf(error_message, "cannot access file '%s'", filename);
        else
            asprintf(error_message, "stat(2) failed for file '%s': %s (errno = %d)", filename, strerror(errno), errno);
        
        return 1;
    }
    
    return 0;
}

/* Try to open the given file and return it's descriptor via 'fd' parameter */
int open_file(int *fd, const char *filename, char **error_message)
{
    if (fd == NULL)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "null fd pointer passed for open_file");
        }

        return 1;
    }
    
    *fd = open(filename, O_RDONLY);
    if (*fd < 0)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);

            if (errno == EACCES)
                asprintf(error_message, "cannot access file '%s'", filename);
            else
                asprintf(error_message, "open(2) failed for file: '%s': %s (errno = %d)", filename, strerror(errno), errno);
        }

        return 1;
    }

    return 0;
}

/* Repeatedly read a chunk of data from 'fd' stream and write it in 'stream_fd' in a loop */
int send_file(int fd, int stream_fd, const char *filename, char **error_message)
{
    char buffer[1024];
    const char* headers = "HTTP/1.1 200 OK\r\n"
                          "Content-type: text/html\r\n\r\n";
    write(stream_fd, headers, strlen(headers));
    
    for (;;)
    {
        ssize_t bread = read(fd, buffer, sizeof(buffer));
        if (bread < 0)
        {
            if (error_message != NULL)
            {
                if (*error_message != NULL)
                    free(error_message);
                asprintf(error_message, "read(2) failed for file '%s' with error: %s (errno = %d)", filename, strerror(errno), errno);
            }

            return 1;
        }

        if (bread == 0)
        {
            break;
        }

        ssize_t bwritten = write(stream_fd, buffer, bread);
        if (bwritten != bread)
        {
            if (error_message != NULL)
            {
                if (*error_message != NULL)
                    free(error_message);
                asprintf(error_message, "write(2) failed for file '%s' with error: %s (errno = %d)", filename, strerror(errno), errno);
            }

            return 1;
        }
    }
    write(stream_fd, "\n\n", 2);
    
    return 0;
}

void close_file(int *fd)
{
    if (fd == NULL || *fd < 0)
        return;

    close(*fd);
    *fd = -1;
}
