
typedef struct tokenizer * tokenizer_t;

int make_tokenizer(int fd, tokenizer_t *tok, char **error_message);
void free_tokenizer(tokenizer_t *tok);

struct token
{
    const char *text;
    unsigned text_length;
};

int read_token_exact(tokenizer_t tok, struct token *token, unsigned count, char **error_message);
int read_token_until(tokenizer_t tok, struct token *token, char stop_char, char **error_message);
int read_token_until_whitespace(tokenizer_t tok, struct token *token, char **error_message);

