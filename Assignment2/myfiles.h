
int check_file_stats(const char *filename, char **error_message);
int open_file(int *fd, const char *filename, char **error_message);
int send_file(int fd, int stream_fd, const char *filename, char **error_message);
void close_file(int *fd);

