#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <signal.h>
#include <time.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "myfiles.h"
#include "myinput.h"

/* Default config file (may be provided via cmd args in future */
const char* mywebserver_conf_filename = "mywebserver.conf";
/* Use this value in case "base_directory" parameter is not set in "mywebserver.conf" */
const char* default_base_directory = "/";

#define CHILDREN_NUM 15
struct
{
    size_t num;
    pid_t pid[ CHILDREN_NUM ];
} children = {0, {0}};
FILE* logfile = NULL;

void init_log(const char* logfilename)
{
    if (!logfilename)
    {
        exit(EXIT_FAILURE);
    }

    logfile = fopen(logfilename, "a+");
    if (!logfile)
    {
        fputs("Cannot open logfile.\n", stderr);
        exit(EXIT_FAILURE);
    }
}

void weblog(const char* format, ...)
{
    /* Timing: */
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    char* strtime = asctime(timeinfo); strtime[strlen(strtime) - 1] = 0;
    fprintf(logfile, "%s - ", strtime);
    /* Vararg: */
    va_list args;
    va_start(args, format);
    vfprintf(logfile, format, args);
    va_end(args);
    fprintf(logfile, "\n");
    fflush(logfile);
}

/* Configuration parameters as a parsing result of "mywebserver.conf" */
struct conf_params
{
    char *default_url;
    char *base_directory;
    char *listen_port;
    char *logfile;
    char *interface;
};

/* Helper function: copy token value into freshly allocated string */
int load_parameter(char **target, struct token *tk, char **error_message)
{
    *target = malloc(tk->text_length + 1);
    if (*target == 0)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "no memory for parameter value");
        }
        
        return 1;
    }

    memcpy(*target, tk->text, tk->text_length);
    (*target)[tk->text_length] = '\0';

    return 0;
}

/* Read configuration file, parse it and return initialized "conf_params" struct */
int read_conf_params(const char *conf_filename, struct conf_params **params, char **error_message)
{
    *params = NULL;

    /* Ensure file exists and can be accessed */
    if (check_file_stats(conf_filename, error_message) != 0)
        return 1;
    
    /* Initialize empty "conf_params" struct */
    *params = malloc(sizeof(struct conf_params));
    memset(*params, 0, sizeof(struct conf_params));
    if (*params == NULL)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "no memory for params object");
        }

        return 1;
    }
    memset(*params, 0, sizeof(struct conf_params));

    int retcode = 1;
    int fd = -1;
    tokenizer_t tok = NULL;
    struct token tk;

    /* Try to open configuration file and init a tokenizer over it */
    if (open_file(&fd, conf_filename, error_message) != 0)
        goto params_cleanup;

    if (make_tokenizer(fd, &tok, error_message) != 0)
        goto params_cleanup;

    memset(&tk, 0, sizeof(tk));

    enum param_type
    {
        PT_UNKNOWN,
        PT_DEFAULT_URL,
        PT_BASE_DIR,
        PT_LISTEN_PORT,
        PT_LOGFILE,
        PT_INTERFACE
    };
    
    for (;;)
    {
        /* "param" describes what parameter we are processing now */
        enum param_type param = PT_UNKNOWN;

        /* Configuration syntax is "key = value", so read a key token until '=' char encountered */
        if (read_token_until(tok, &tk, '=', error_message) != 0)
            goto params_cleanup;
        if (tk.text_length == 0)
            break;

        /* Try to identify parameter name */
        if (strncmp(tk.text, "DEFAULT_URL", tk.text_length) == 0)
            param = PT_DEFAULT_URL;
        else if (strncmp(tk.text, "BASE_DIRECTORY", tk.text_length) == 0)
            param = PT_BASE_DIR;
        else if (strncmp(tk.text, "LISTEN_PORT", tk.text_length) == 0)
            param = PT_LISTEN_PORT;
        else if (strncmp(tk.text, "LOGFILE", tk.text_length) == 0)
            param = PT_LOGFILE;
        else if (strncmp(tk.text, "INTERFACE", tk.text_length) == 0)
            param = PT_INTERFACE;

        /* Skip '=' token */
        if (read_token_exact(tok, &tk, 1, error_message) != 0)
            goto params_cleanup;
        if (tk.text_length != 1 || tk.text[0] != '=')
        {
            if (error_message != NULL)
            {
                if (*error_message != NULL)
                    free(error_message);
                asprintf(error_message, "expected '=' token for param key");
            }

            goto params_cleanup;
        }

        /* Read parameter value (what is rest on a line until newline char) */
        if (read_token_until(tok, &tk, '\n', error_message) != 0)
            goto params_cleanup;
        if (tk.text_length == 0)
        {
            if (error_message != NULL)
            {
                if (*error_message != NULL)
                    free(error_message);
                asprintf(error_message, "no value provided for parameter key");
            }

            goto params_cleanup;
        }

        /* Check if it was a legal parameter */
        switch (param)
        {
        case PT_UNKNOWN:
            if (error_message != NULL)
            {
                if (*error_message != NULL)
                    free(error_message);
                asprintf(error_message, "unsupported parameter provided in %s", conf_filename);
            }

            goto params_cleanup;

        case PT_DEFAULT_URL:
            /* Default url parameter: just copy value */
            if (load_parameter(&(*params)->default_url, &tk, error_message) != 0)
                goto params_cleanup;
            break;

        case PT_BASE_DIR:
            /* Base directory parameter: if it's value is '.' (current directory), resolve it via "getcwd" */
            if (tk.text_length == 1 && tk.text[0] == '.')
                (*params)->base_directory = getcwd(NULL, 0);
            else if (load_parameter(&(*params)->base_directory, &tk, error_message) != 0)
                goto params_cleanup;
            break;

        case PT_LISTEN_PORT:
            if (load_parameter(&(*params)->listen_port, &tk, error_message) != 0)
                goto params_cleanup;
            break;

        case PT_LOGFILE:
            if (load_parameter(&(*params)->logfile, &tk, error_message) != 0)
                goto params_cleanup;
            break;

        case PT_INTERFACE:
            if (load_parameter(&(*params)->interface, &tk, error_message) != 0)
                goto params_cleanup;
            break;
        }
    }

    /* Default url should be provided according the task */
    if ((*params)->default_url == NULL)
    {
        if (error_message != NULL)
        {
            if (*error_message != NULL)
                free(error_message);
            asprintf(error_message, "no default_url provided in '%s'", conf_filename);
        }

        goto params_cleanup;
    }

    /* Use default base directory in case it was not provided */
    if ((*params)->base_directory == NULL)
        (*params)->base_directory = strdup(default_base_directory);
        
    retcode = 0;
    
params_cleanup:
    free_tokenizer(&tok);
    close_file(&fd);
    return retcode;
}

/* Accurately deallocate parameters object */
void free_params(struct conf_params **params)
{
    if ((params == NULL) || (*params == NULL))
        return;
    if ((*params)->default_url != NULL)
        free((*params)->default_url);
    if ((*params)->base_directory != NULL)
        free((*params)->base_directory);
    free(*params);
    *params = NULL;
}

/* Copy "filename" contents to "stream_fd" provided */
int http_sendfile(const char *filename, int stream_fd, char **error_message)
{
    int retcode = 1;
    int fd = -1;
    if (open_file(&fd, filename, error_message) != 0)
    {
        retcode = 1;
        goto sendfile_cleanup;
    }

    if (send_file(fd, stream_fd, filename, error_message) != 0)
    {
        retcode = 2;
        goto sendfile_cleanup;
    }
    
    retcode = 0;
    
sendfile_cleanup:
    close_file(&fd);
    return retcode;
}

/* Sockets stuff */
int sockfd;
int new_fd;
struct sockaddr_in their_addr;
socklen_t sin_size;
#define BUFLEN 8196

int serve_client(struct conf_params* params)
{
    char* error_message = NULL;
    char input[ BUFLEN ];
    memset(input, 0, BUFLEN);

    ssize_t bread = read(new_fd, input, BUFLEN);
    char filename[ 124 ] = {0};
    if (bread < 0)
    {
        return -1;
    }
    char* lines = strtok(input, "\n\r");
    char* cmd = strtok(lines, " ");

    if (cmd && strcmp(cmd, "GET") == 0)
    {
        char *name = strtok(NULL, " ");
        strcat(filename, params->base_directory);
        strcat(filename, name);
        if (strlen(name) == 1 && name[0] == '/')
        {
            strcat(filename, params->default_url);
        }

        int code;
        if (!(code = http_sendfile(filename, new_fd, &error_message)))
        {
            weblog("%s(%s)", cmd, name);
        }
        else if (code == 1)
        {
            weblog("ERROR %s(%s) NO SUCH FILE", cmd, name);
        }
        else if (code == 2)
        {
            weblog("ERROR %s(%s) CAN NOT READ THE FILE", cmd, name);
        }
    }
    else
    {
        puts("Error: unsupported command.");
    }

    close(new_fd);
    exit(EXIT_SUCCESS);
}

int create_and_bind(int portf, struct conf_params* params)
{
    int yes = 1;
    struct sockaddr_in my_addr;

    if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(portf);
    my_addr.sin_addr.s_addr = (!params->interface)
                              ? INADDR_ANY
                              : inet_addr(params->interface);
    memset(&(my_addr.sin_zero), '\0', 8);

    if (!~bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))) 
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    if (!~listen(sockfd, SOMAXCONN))
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //sa.sa_handler = sigchld_handler;
    //sigemptyset(&sa.sa_mask);
    //sa.sa_flags = SA_RESTART;
    //if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    //    perror("sigaction");
    //    exit(EXIT_FAILURE);
    //}
    return sockfd;
}

void quit_term_handler(int sig)
{
    size_t i;
    for (i = 0; i < children.num; ++i)
    {
        printf("SIGTERM to the %ld child.\n", (long) children.pid[ i ]);
        kill(children.pid[ i ], SIGTERM);
    }
    const char* m = "Caught quit/term signal.";
    printf("sig[%d] %s\n", sig, m);
    weblog(m);
    fclose(logfile);
    exit(EXIT_SUCCESS);
}

struct conf_params *params = NULL;

void sighup_handler(int sig)
{
    size_t i;
    char* error_message = NULL;
    for (i = 0; i < children.num; ++i)
    {
        printf("SIGTERM to the %ld child.\n", (long) children.pid[ i ]);
        kill(children.pid[ i ], SIGTERM);
    }
    const char* m = "Caught sighup signal.";
    printf("sig[%d] %s\n", sig, m);
    weblog(m);
    fclose(logfile);
    if (read_conf_params(mywebserver_conf_filename,
                         &params, &error_message) != 0)
    {
        exit(EXIT_FAILURE);
    }
    init_log(params->logfile);
    weblog("Restarting server.");
}

int main(int argc, char **argv)
{
    int retcode = 1;
    int connfd;
    char *error_message = NULL;

    signal(SIGHUP, sighup_handler);
    signal(SIGQUIT, quit_term_handler);
    signal(SIGTERM, quit_term_handler);

    /* Read parameters just after program start */
    if (read_conf_params(mywebserver_conf_filename,
                         &params,
                         &error_message) != 0)
    {
        goto cleanup;
    }

    init_log(params->logfile);
    weblog("Starting server.");

    int portf = atoi(params->listen_port);
    create_and_bind(portf, params);

    while ((new_fd = accept(sockfd,
                            (struct sockaddr *) &their_addr,
                            &sin_size)) > 0)
    {
        if (!(/*children.pid[ children.num++ ] = */fork()))
        {
            retcode = serve_client(params);
        }
        close(new_fd);
    }

    retcode = 0;
    
cleanup:
    if (retcode != 0 && error_message != 0)
        fprintf(stderr, "Error: %s\n", error_message);

    free_params(&params);
    if (error_message != NULL)
        free(error_message);
    
    return retcode;
}
